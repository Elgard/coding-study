﻿using UnityEngine;
using Random = System.Random;

namespace Assets
{

    /*
        bool    - Логический, предоставляет два значения: “истина” или “ложь”
        byte    - 8-разрядный целочисленный без знака
        char    - Символьный
        decimal - Десятичный (для финансовых расчетов)
        double  - С плавающей точкой двойной точности
        float   - С плавающей точкой одинарной точности
        int     - Целочисленный
        long    - Длинный целочисленный
        sbyte   - 8-разрядный целочисленный со знаком
        short   - Короткий целочисленный
        uint    - Целочисленный без знака
        ulong   - Длинный целочисленный без знака
        ushort  - Короткий целочисленный без знака
     */
    /*
        Тип     биты    минимальное значение           максимальное значение
        byte     8      0                               255
        sbyte    8      -128                            127
        short   16      -32 768                         32 767
        ushort  16      0                               65 535
        int     32      -2 147 483 648                  2 147 483 647
        uint    32      0                               4 294 967 295
        long    64      -9 223 372 036 854 775 808      9 223 372 036 854 775 807
        ulong   64      0                               18 446 744 073 709 551 615
     */
    class Variables
    {
        //константы
        public const int ClassId = 5;
        const string Key = "ClassKey";

        //статические переменные
        private static float StaticVariable;

        //переменные экземпляра
        protected bool IsVariable = true;

        //без позвращаемого значения без параметров
        public void PrintKey()
        {
            Debug.Log(Key);//вызов статической функции и передача туда значения типа стринг
        }
        //возвращает int, без параметров
        public int GetRandomValue()
        {
            var random = new Random();
            return random.Next();
        }

        private static void SetVariable(float value)
        {
            StaticVariable = value;
        }

        public void TestOperators()
        {
            //add
            int addRezelt = ClassId + 3;

        }

    }
}
