﻿namespace Assets._01_Lesson
{
    /* арифметические операции
        +    Сложение
        -    Вычитание
        *    Умножение
        /    Деление
        %    Деление по модулю
        --   Декремент
        ++   Инкремент
     */

    /* логические операции
        <   Меньше
        <=  Меньше или равно
        >   Больше
        >=  Больше или равно
        ==  Равно
        !=  Не равно
        &   И
        |   ИЛИ
        ^   Исключающее ИЛИ
        &&  Укороченное И
        ||  Укороченное ИЛИ
        !   НЕ
     */

    /*
     Ключевого слова static. Метод, определяемый ключевым
     словом static, может вызываться без создания объекта его класса.
     */
    static class MathOperation
    {
        public static float GetSquare(float value)
        {
            return value * value;
        }

        public static float CircleLenght(float radius)
        {
            const float pi = 3.14f;
            return 2 * pi * radius;
        }

        public static int Increment(int value)
        {
            value++;
            return value;
        }
    }

    public static class BooleanOperation
    {
        public static int Compare(float value1, float value2)
        {
            if (value1 < value2)
            {
                return -1;
            }

            if (value1 > value2)
            {
                return 1;
            }

            return 0;
        }

        public static bool Any(bool value1, bool value2)
        {
            return value1|| value2;
        }
        public static bool All(bool value1, bool value2)
        {
            return value1 && value2;
        }
    }
}
